var socket = new WebSocket("ws://localhost:8080/chat");

socket.onmessage = function(event) {
    var chatMessages = document.getElementById("chat-messages");
    chatMessages.innerHTML += "<div class='message'>" + event.data + "</div>";
    chatMessages.scrollTop = chatMessages.scrollHeight;
};

function sendMessage() {
    var messageInput = document.getElementById("message-input");
    var message = messageInput.value.trim();
    if (message !== "") {
        socket.send(message);
        messageInput.value = "";
    }
}

document.getElementById("send-button").addEventListener("click", sendMessage);
document.getElementById("message-input").addEventListener("keypress", function(event) {
    if (event.key === "Enter") {
        sendMessage();
    }
});
