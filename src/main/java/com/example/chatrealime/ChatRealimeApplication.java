package com.example.chatrealime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatRealimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatRealimeApplication.class, args);
	}

}
